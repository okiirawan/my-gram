package database

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"log"
	"myGram/models"
	"os"
	"strconv"
)

var Db *gorm.DB

type DBConfig struct {
	Host       string
	Port       int
	User       string
	DBDatabase string
	Password   string
}

func InitDB() {
	db, err := gorm.Open(postgres.Open(PostgresConfig(InsertConfig())), &gorm.Config{})

	if err != nil {
		log.Fatal("error connecting to database :", err)
	}

	db.Debug().AutoMigrate(&models.User{}, &models.Comment{}, &models.Photo{}, &models.SocialMedia{})
	// See "Important settings" section.
}

func InsertConfig() *DBConfig {
	port, _ := strconv.Atoi(os.Getenv("DB_PORT"))
	dbConfig := DBConfig{
		Host:       os.Getenv("DB_HOST"),
		Port:       port,
		User:       os.Getenv("DB_USER"),
		DBDatabase: os.Getenv("DB_DATABASE"),
		Password:   os.Getenv("DB_PASSWORD"),
	}
	return &dbConfig
}

func PostgresConfig(dbConfig *DBConfig) string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.User,
		dbConfig.Password,
		dbConfig.DBDatabase,
	)
}

func GetDB() *gorm.DB {
	return Db
}
