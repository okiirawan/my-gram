package main

import (
	"github.com/joho/godotenv"
	"log"
	"myGram/database"
	"myGram/router"
)

func main() {
	e := godotenv.Load()
	if e != nil {
		log.Fatal("Error loading .env file: ", e)
	}

	database.InitDB()

	r := router.InitRouter()
	r.Run(":8080")
}
