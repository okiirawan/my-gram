package controllers

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"myGram/database"
	"myGram/helpers"
	"myGram/models"
	"net/http"
	"strconv"
)

func GetAllPhotos(c *gin.Context) {
	db := database.GetDB()
	photos := []models.Photo{}

	err := db.Debug().Preload("User").Order("id asc").Find(&photos).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	photosString, _ := json.Marshal(photos)
	photosResponse := []models.PhotoResponse{}
	errMarshal := json.Unmarshal(photosString, &photosResponse)
	if err != nil {
		panic(errMarshal)
		return
	}

	c.JSON(http.StatusOK, photosResponse)

}

func GetPhoto(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)

	photoId, _ := strconv.Atoi(c.Param("photoId"))
	userId := uint(userData["id"].(float64))

	photo := models.Photo{}
	photo.ID = uint(photoId)
	photo.UserId = userId

	err := db.Debug().First(&photo).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	photoString, _ := json.Marshal(&photo)
	photoResponse := models.PhotoResponse{}
	errMarshal := json.Unmarshal(photoString, &photoResponse)

	if err != nil {
		panic(errMarshal)
		return
	}

	c.JSON(http.StatusOK, photoResponse)
}

func CreatePhoto(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)

	photoRequest := models.CreatePhotoRequest{}
	userID := uint(userData["id"].(float64))

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&photoRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&photoRequest); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}

		photo := models.Photo{
			Title:    photoRequest.Title,
			Caption:  photoRequest.Caption,
			PhotoUrl: photoRequest.PhotoUrl,
			UserId:   userID,
		}

		err := db.Debug().Create(&photo).Error
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		}

		_ = db.First(&photo, photo.ID).Error

		photoString, _ := json.Marshal(photo)
		photoResponse := models.CreatePhotoResponse{}
		json.Unmarshal(photoString, &photoResponse)

		c.JSON(http.StatusCreated, photoResponse)
	}
}

func UpdatePhoto(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)

	photoUpdateRequest := models.UpdatePhotoRequest{}
	photoId, _ := strconv.Atoi(c.Param("photoId"))
	userId := uint(userData["id"].(float64))

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&photoUpdateRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&photoUpdateRequest); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}
	}

	photo := models.Photo{}
	photo.ID = uint(photoId)
	photo.UserId = userId

	updatePhotoString, _ := json.Marshal(photoUpdateRequest)
	updatePhotoData := models.Photo{}
	json.Unmarshal(updatePhotoString, &updatePhotoData)

	err := db.Debug().Model(&photo).Updates(updatePhotoData).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	photoString, _ := json.Marshal(photo)
	photoResponse := models.UpdatePhotoResponse{}
	json.Unmarshal(photoString, &photoResponse)

	c.JSON(http.StatusOK, photoResponse)
}

func DeletePhoto(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)

	photoId, _ := strconv.Atoi(c.Param("photoId"))
	userId := uint(userData["id"].(float64))

	photo := models.Photo{}
	photo.ID = uint(photoId)
	photo.UserId = userId

	err := db.Debug().Delete(&photo).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "The photo has been successfully deleted",
	})
}
