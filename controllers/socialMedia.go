package controllers

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"myGram/database"
	"myGram/helpers"
	"myGram/models"
	"net/http"
	"strconv"
)

func GetAllSocialMedia(c *gin.Context) {
	db := database.GetDB()
	socialMedias := []models.SocialMedia{}

	err := db.Debug().Preload("User").Order("id asc").Find(&socialMedias).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	socialMediasString, _ := json.Marshal(socialMedias)
	socialMediasResponse := []models.SocialMediaResponse{}
	json.Unmarshal(socialMediasString, &socialMediasResponse)

	c.JSON(http.StatusOK, socialMediasResponse)
}

func GetSocialMedia(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)

	socialMediaId, _ := strconv.Atoi(c.Param("socialMediaId"))
	userID := uint(userData["id"].(float64))

	socialMedia := models.SocialMedia{}
	socialMedia.ID = uint(socialMediaId)
	socialMedia.UserId = userID

	err := db.Debug().First(&socialMedia).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	socialMediaString, _ := json.Marshal(&socialMedia)
	socialMediaResponse := models.SocialMediaResponse{}
	json.Unmarshal(socialMediaString, &socialMediaResponse)

	c.JSON(http.StatusOK, socialMediaResponse)

}

func CreateSocialMedia(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)

	socialMediaReq := models.CreateSocialMediaRequest{}
	userId := uint(userData["id"].(float64))

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&socialMediaReq); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&socialMediaReq); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}
	}

	socialMedia := models.SocialMedia{
		Name:           socialMediaReq.Name,
		SocialMediaUrl: socialMediaReq.SocialMediaUrl,
		UserId:         userId,
	}

	err := db.Debug().Create(&socialMedia).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	_ = db.First(&socialMedia, socialMedia.ID).Error

	socialMediaString, _ := json.Marshal(socialMedia)
	socialMediaResponse := models.CreateSocialMediaResponse{}
	json.Unmarshal(socialMediaString, &socialMediaResponse)

	c.JSON(http.StatusOK, socialMediaResponse)
}

func UpdateSocialMedia(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)
	contentType := helpers.GetContentType(c)

	userId := uint(userData["id"].(float64))
	socialMediaId, _ := strconv.Atoi(c.Param("socialMediaId"))
	socialMediaReq := models.UpdateSocialMediaRequest{}

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&socialMediaReq); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&socialMediaReq); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}
	}

	socialMedia := models.SocialMedia{}
	socialMedia.ID = uint(socialMediaId)
	socialMedia.UserId = userId

	updateSocialMediaString, _ := json.Marshal(socialMediaReq)
	updateSocialMediaData := models.SocialMedia{}
	json.Unmarshal(updateSocialMediaString, &updateSocialMediaData)

	err := db.Debug().Model(&socialMedia).Updates(updateSocialMediaData).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	socialMediaString, _ := json.Marshal(socialMedia)
	socialMediaUpdateRes := models.UpdateSocialMediaResponse{}
	json.Unmarshal(socialMediaString, &socialMediaUpdateRes)

	c.JSON(http.StatusOK, socialMediaUpdateRes)
}

func DeleteSocialMedia(c *gin.Context) {
	db := database.GetDB()
	userData := c.MustGet("userData").(jwt.MapClaims)

	socialMediaId, _ := strconv.Atoi(c.Param("socialMediaId"))
	userId := uint(userData["id"].(float64))

	socialMedia := models.SocialMedia{}
	socialMedia.ID = uint(socialMediaId)
	socialMedia.UserId = userId

	err := db.Debug().Delete(&socialMedia).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "The Social Media has been successfully deleted",
	})
}
