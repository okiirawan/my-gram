package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"myGram/database"
	"myGram/helpers"
	"myGram/models"
	"net/http"
)

var appJSON = "application/json"

func UserRegister(c *gin.Context) {
	db := database.GetDB()
	contentType := helpers.GetContentType(c)

	userRequest := models.UserRegisterRequest{}

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&userRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&userRequest); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}
	}

	user := models.User{
		Username: userRequest.Username,
		Email:    userRequest.Email,
		Password: helpers.HashPassword(userRequest.Password),
		Age:      userRequest.Age,
	}

	err := db.Debug().Create(&user).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   "Bad Request",
			"message": err.Error(),
		})
		return
	}

	userString, _ := json.Marshal(user)
	userResponse := models.UserRegisterResponse{}
	errMarshal := json.Unmarshal(userString, &userResponse)
	if err != nil {
		panic(errMarshal)
		return
	}

	c.JSON(http.StatusCreated, userResponse)

}

func UserLogin(c *gin.Context) {
	db := database.GetDB()
	contentType := helpers.GetContentType(c)

	userLoginRequest := models.UserLoginRequest{}

	if contentType == appJSON {
		if err := c.ShouldBindJSON(&userLoginRequest); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error":   "Bad Request",
				"message": err.Error(),
			})
			return
		} else {
			if err := c.ShouldBind(&userLoginRequest); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{
					"error":   "Bad Request",
					"message": err.Error(),
				})
				return
			}
		}
	}

	password := userLoginRequest.Password
	user := models.User{}

	// Check if there is user ini database
	err := db.Debug().Where("username = ?", userLoginRequest.Username).Take(&user).Error
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error":   "Unauthorized",
			"message": "Invalid email/password",
		})
		return
	}
	comparePass := helpers.ComparePassword([]byte(user.Password), []byte(password))

	if !comparePass {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error":   "Unauthorized",
			"message": "Invalid email/password",
		})
		return
	}

	token := helpers.GenerateToken(user.ID, user.Username)

	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}
