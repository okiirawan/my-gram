package models

import "time"

type Comment struct {
	GormModel
	UserId  uint `json:"user_id"`
	User    *User
	PhotoId uint `json:"photo_id"`
	Photo   *Photo
	Message string `gorm:"not null" json:"message" form:"message" valid:"required"`
}

type CreateCommentRequest struct {
	PhotoId uint   `json:"photo_id" form:"photo_id"`
	Message string `gorm:"not null" json:"message" form:"message" valid:"required"`
}

type UpdateCommentRequest struct {
	Message string `gorm:"not null" json:"message" form:"message" valid:"required"`
}

type CreateCommentResponse struct {
	ID        uint       `json:"id"`
	Message   string     `json:"message"`
	UserId    uint       `json:"userId"`
	PhotoId   uint       `json:"photoId"`
	CreatedAt *time.Time `json:"created_at"`
}

type UpdateCommentResponse struct {
	ID        uint       `json:"id"`
	Message   string     `json:"message"`
	UserId    uint       `json:"userId"`
	PhotoId   uint       `json:"photoId"`
	UpdatedAt *time.Time `json:"updated_at"`
}

type CommentResponse struct {
	ID        uint       `json:"id"`
	Message   string     `json:"message"`
	UserId    uint       `json:"userId"`
	PhotoId   uint       `json:"photoId"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	User      *UserCommentResponse
	Photo     *PhotoCommentResponse
}
