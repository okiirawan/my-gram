package models

type User struct {
	GormModel
	Username     string        `gorm:"not null;uniqueIndex" json:"username" form:"username" valid:"required~username is required"`
	Email        string        `gorm:"not null;uniqueIndex" json:"email" valid:"email,required~Your full email is required~Invalid email format"`
	Password     string        `gorm:"not null" json:"password" form:"password" valid:"required~Your password is required,minstringlength(6)~Password minimum length of 6"`
	Age          int           `gorm:"not null" json:"age" form:"age" valid:"required,range(8,99)"`
	Photos       []Photo       `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"-"`
	Comments     []Comment     `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"-"`
	SocialMedias []SocialMedia `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"-"`
}

type UserRegisterRequest struct {
	Username string `gorm:"not null;uniqueIndex" json:"username" form:"username" valid:"required~username is required"`
	Email    string `gorm:"not null;uniqueIndex" json:"email" valid:"email,required~Your full email is required~Invalid email format"`
	Password string `gorm:"not null" json:"password" form:"password" valid:"required~Your password is required,minstringlength(6)~Password minimum length of 6"`
	Age      int    `gorm:"not null" json:"age" form:"age" valid:"required,range(8,99),min(8)"`
}

type UserRegisterResponse struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Age      uint   `json:"age"`
}

type UserLoginRequest struct {
	Username string `gorm:"not null;uniqueIndex" json:"username" form:"username" valid:"required~username is required"`
	Password string `gorm:"not null" json:"password" form:"password" valid:"required~Your password is required,minstringlength(6)~Password minimum length of 6"`
}

type UserPhotoResponse struct {
	Email    string `json:"email"`
	Username string `json:"username"`
}

type UserCommentResponse struct {
	ID       uint   `json:"id"`
	Email    string `json:"email"`
	Username string `json:"username"`
}

type UserSocialMediaResponse struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
}
