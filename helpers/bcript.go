package helpers

import "golang.org/x/crypto/bcrypt"

func HashPassword(plainPassword string) string {
	salt := 8
	passwordByte := []byte(plainPassword)
	hash, _ := bcrypt.GenerateFromPassword(passwordByte, salt)

	return string(hash)
}

func ComparePassword(hash, password []byte) bool {
	hashByte, passwordByte := []byte(hash), []byte(password)

	err := bcrypt.CompareHashAndPassword(hashByte, passwordByte)

	return err == nil
}
